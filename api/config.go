package api

import "time"

type ApiConfiguration struct {
	ResponseDelay time.Duration `yaml:"response-delay"`
	ResponseSize  int           `yaml:"response-size"`
}
